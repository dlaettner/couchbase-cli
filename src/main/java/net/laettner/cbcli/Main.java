package net.laettner.cbcli;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import net.laettner.cbcli.dal.CouchBaseDAO;
import net.laettner.cbcli.dal.beans.ConnectionBean;
import net.laettner.cbcli.enums.Keywords;
import net.laettner.cbcli.exceptions.ConnectionParamsAreNotSetException;
import net.laettner.cbcli.exceptions.NotEnoughArgumentsException;
import net.laettner.cbcli.handlers.KeywordHandlerInitiator;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by dlaettner on 04/11/2016.
 */
public class Main {
    Scanner input;
    private boolean prettyPrint = false;

    private List<String> keywords = new ArrayList<String>();

    public void run(ConnectionBean connectionBean){
        try {
            System.out.println("CouchBaseClient v0.1");
            System.out.println("====================");
            CouchBaseDAO couchBaseDAO = new CouchBaseDAO(connectionBean);
            System.out.println("connecting to " + connectionBean.getHosts()[0]
                    + "/" + connectionBean.getBucketName());
            couchBaseDAO.createBucketConnection();
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String cmd = null;
            input = new Scanner(System.in);
            do {
                    System.out.print(">>");
                    cmd = input.next();

                    if(!cmd.isEmpty()) {
                        if(Keywords.isKeyword(cmd)){
                            handleKeyword(Keywords.getKeyword(cmd));
                        } else {
                            JsonObject byKey = couchBaseDAO.findByKey(cmd);
                            if(null == byKey) {
                                System.out.println("key " + byKey + " not found");
                            } else {
                                if(prettyPrint)
                                    System.out.println(gson.toJson(byKey));
                                else
                                    System.out.println(byKey);
                            }
                        }
                    }
            } while(!stillQuerying(cmd));
            System.out.println("bye");
        } catch (ConnectionParamsAreNotSetException e) {
            e.printStackTrace();
        }
    }

    private boolean stillQuerying(String cmd) {
        return (Keywords.EXIT.toString().equals(cmd.trim()) ||
                Keywords.QUIT.toString().equals(cmd.trim()));
    }

    private void handleKeyword(Keywords keyword) {
        KeywordHandlerInitiator.init(keyword);
    }

    public static void main(String[] args) throws ParseException, NotEnoughArgumentsException {
        Main main = new Main();
        Options options = main.setupOptions();
        ConnectionBean connectionBean = main.processCommandLine(options, args);
        main.run(connectionBean);
    }

    private Options setupOptions(){
        Options options = new Options();
        options.addOption(new Option("h","hosts",true,"hosts addresses"));
        options.addOption(new Option("b","bucket-name",true,"bucket name"));
        options.addOption(new Option("p","password",true,"bucket password"));
        options.addOption(new Option("v","pretty",false,"JSON pretty print"));
        return options;
    }

    private ConnectionBean processCommandLine(Options opts, String[] args)
            throws ParseException, NotEnoughArgumentsException {
        CommandLineParser parser = new BasicParser();
        CommandLine cmd = parser.parse( opts, args );
        if(null == cmd.getOptionValue("b") || null == cmd.getOptionValue("h")){
            printUsage(opts);
            throw new NotEnoughArgumentsException("No all arguments were set");
        }
        if(cmd.hasOption("v"))
            prettyPrint = true;

        return new ConnectionBean(
                cmd.getOptionValue("b"),
                (null == cmd.getOptionValue("p")) ? "" :  cmd.getOptionValue("p") ,
                cmd.getOptionValue("h").split(":")
        );
    }

    private void printUsage(Options opts) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp( "CouchBaseClient", opts );
    }
}
