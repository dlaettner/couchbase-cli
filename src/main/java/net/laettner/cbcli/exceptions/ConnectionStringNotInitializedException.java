package net.laettner.cbcli.exceptions;

/**
 * Created by dlaettner on 31/12/15.
 */
public class ConnectionStringNotInitializedException extends Exception {
    public ConnectionStringNotInitializedException() {
        super();
    }

    public ConnectionStringNotInitializedException(String message) {
        super(message);
    }
}
