package net.laettner.cbcli.exceptions;

/**
 * Created by dlaettner on 17/03/16.
 */
public class NotEnoughArgumentsException extends Exception {
    public NotEnoughArgumentsException(String message) {
        super(message);
    }
}
