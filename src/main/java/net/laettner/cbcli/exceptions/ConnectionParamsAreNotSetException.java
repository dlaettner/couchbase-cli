package net.laettner.cbcli.exceptions;

/**
 * Created by dlaettner on 17/03/16.
 */
public class ConnectionParamsAreNotSetException extends Exception {
    public ConnectionParamsAreNotSetException(String message) {
        super(message);
    }
}
