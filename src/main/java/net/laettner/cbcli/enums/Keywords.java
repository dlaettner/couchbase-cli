package net.laettner.cbcli.enums;

import java.util.HashMap;
import java.util.Map;

public enum Keywords {

    EXIT("exit"),
    QUIT(".q"),
    HELP("help");

    private String keyword;

    private static Map<String,Keywords> keywordsMap;

    static {
        keywordsMap = new HashMap<String, Keywords>();
        for (Keywords k : Keywords.values())
            keywordsMap.put(k.toString(),k);
    }

    Keywords(String keyword){
        this.keyword  = keyword;
    }

    public static boolean isKeyword(String keyword){
        return keywordsMap.containsKey(keyword);
    }

    public static Keywords getKeyword(String keyword) {
        return keywordsMap.get(keyword);
    }

    @Override
    public String toString(){
        return keyword;
    }
}
