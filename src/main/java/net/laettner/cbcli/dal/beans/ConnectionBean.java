package net.laettner.cbcli.dal.beans;

/**
 * Created by dlaettner on 05/11/2016.
 */
public class ConnectionBean {

    private String bucketName;
    private String password;
    private String[] hosts;

    public ConnectionBean(String bucketName, String password, String[] hosts) {
        this.bucketName = bucketName;
        this.password = password;
        this.hosts = hosts;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String[] getHosts() {
        return hosts;
    }

    public void setHosts(String[] hosts) {
        this.hosts = hosts;
    }
}
