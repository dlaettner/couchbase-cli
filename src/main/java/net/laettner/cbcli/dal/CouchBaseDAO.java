package net.laettner.cbcli.dal;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.document.StringDocument;
import net.laettner.cbcli.dal.beans.ConnectionBean;
import net.laettner.cbcli.exceptions.ConnectionParamsAreNotSetException;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by dlaettner on 20/01/16.
 *
 * This class connect the couchbase db
 */
public class CouchBaseDAO {

    public CouchBaseDAO() {
    }

    public CouchBaseDAO(ConnectionBean connectionBean){
        this.bucketName = connectionBean.getBucketName();
        this.password = connectionBean.getPassword();
        this.hosts = connectionBean.getHosts();
    }

    public CouchBaseDAO(String bucketName, String password, String[] hosts) {
        this.bucketName = bucketName;
        this.password = password;
        this.hosts = hosts;
    }

    public CouchBaseDAO(Bucket bucket) {
        this.bucket = bucket;
    }

    private Bucket bucket;
    private String bucketName;
    private String password;
    private String[] hosts;

    private static Map<String,Bucket> openBucketsMap = new ConcurrentHashMap<String, Bucket>();

    public CouchBaseDAO setBucketName(String bucketName) {
        this.bucketName = bucketName;
        return this;
    }

    public CouchBaseDAO setPassword(String password) {
        this.password = password;
        return this;
    }

    public CouchBaseDAO setHosts(String[] hosts) {
        this.hosts = hosts;
        return this;
    }

    public void createBucketConnection() throws ConnectionParamsAreNotSetException {
        bucket = getBucket(bucketName);
    }

    public JsonObject findByKey(String key) throws ConnectionParamsAreNotSetException {
        bucket = getBucket(bucketName);
        StringDocument response = bucket.get(key,StringDocument.class,90000L, TimeUnit.SECONDS);
        JsonObject retJson = null;
        if (null != response) {
            retJson = (JsonObject) new JsonParser().parse(response.content().toString());
        }
        return retJson;
    }

    private Bucket getBucket(String bucketName) throws ConnectionParamsAreNotSetException {
        if(!checkConnectionParamsAreSet()){
            String msgError = getErrorMessage();
            throw new ConnectionParamsAreNotSetException(msgError);
        }
        Bucket retBucket;
        if(openBucketsMap.containsKey(bucketName)){
            retBucket = openBucketsMap.get(bucketName);
        } else {
            retBucket = CouchBaseConnection.getBucketConnection(bucketName, password, hosts);
            openBucketsMap.put(bucketName,retBucket);
        }
        return retBucket;
    }

    private boolean checkConnectionParamsAreSet(){
        return !(this.bucketName == null ||
                this.password == null ||
                this.hosts.length == 0);
    }

    private String getErrorMessage(){
        String errMsg = "Couchbase connection params are not set => ";
        errMsg += " bucketName : " + ((this.bucketName == null)? "null":
                                    (this.bucketName.isEmpty())?"[empty]":this.bucketName);
        errMsg += "; password : " + ((this.password == null)? "null":
                                    (this.password.isEmpty())?"[empty]":this.password);
        errMsg += "; hosts :" + ((this.hosts == null) ? "null" :
                                (this.hosts.length == 0) ? "[empty]":this.hosts.toString());
        return errMsg;
    }

    public boolean isBucketOpen(String bucketName){
        return openBucketsMap.containsKey(bucketName);
    }

    public void closeAllOpenBuckets(){
        for(Bucket openBucket : openBucketsMap.values()){
            if(!openBucket.isClosed()){
                openBucket.close();
            }
        }
    }
}