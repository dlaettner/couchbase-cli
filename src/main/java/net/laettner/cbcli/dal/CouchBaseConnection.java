package net.laettner.cbcli.dal;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;

/**
 * Created by dlaettner on 20/01/16.
 */

public class CouchBaseConnection {

    private static Cluster cluster = null;

    public static Bucket getBucketConnection(String bucketName, String password, String...hosts){
        CouchbaseEnvironment env = DefaultCouchbaseEnvironment.builder()
                .connectTimeout(900000)
                .build();
        if (null == cluster) {
            cluster = CouchbaseCluster.create(env, hosts);
        }
        return cluster.openBucket(bucketName, password);
    }
}
