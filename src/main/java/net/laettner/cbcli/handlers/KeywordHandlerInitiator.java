package net.laettner.cbcli.handlers;

import net.laettner.cbcli.enums.Keywords;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dlaettner on 05/11/2016.
 */
public class KeywordHandlerInitiator {

    private static Map<Keywords,KeywordHandler> keywordHandlerMap;

    static {
        keywordHandlerMap = new HashMap<Keywords,KeywordHandler>();
        keywordHandlerMap.put(Keywords.HELP, new HelpKeywordHandler());
        keywordHandlerMap.put(Keywords.EXIT, new ExitKeywordHandler());
        keywordHandlerMap.put(Keywords.QUIT, new ExitKeywordHandler());
    }

    public static void init(Keywords keyword){
        keywordHandlerMap.get(keyword).handleKeyword();
    }
}