package net.laettner.cbcli.handlers;

/**
 * Created by dlaettner on 05/11/2016.
 */
public interface KeywordHandler {
    void handleKeyword();
}
