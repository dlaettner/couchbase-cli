package net.laettner.cbcli.handlers;

/**
 * Created by dlaettner on 05/11/2016.
 */
public class HelpKeywordHandler implements KeywordHandler {

    @Override
    public void handleKeyword() {
        System.out.println("Help\n=====");
        System.out.println("*to get a document enter the key and press enter");
        System.out.println("************************************************\n\n");
    }
}
